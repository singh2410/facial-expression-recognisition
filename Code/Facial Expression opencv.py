#!/usr/bin/env python
# coding: utf-8

# # Facial Expression Recognition Testing File
# #By- Aarush Kumar
# #Dated: May 29,2021

# In[4]:


get_ipython().system('pip install keras')
get_ipython().system('pip install tensorflow')


# In[1]:


from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.preprocessing import image
import cv2
import numpy as np


# In[2]:


face_classifier=cv2.CascadeClassifier(r'/home/aarush100616/Downloads/Projects/facialexpressionrec/haarcascade_frontalface_default.xml')


# In[3]:


classifier=load_model(r'/home/aarush100616/Downloads/Projects/facialexpressionrec/model.h5')


# In[4]:


class_labels=['Angry','Happy','Sad','Neutral','Surprise']


# In[ ]:


cap=cv2.VideoCapture(0)
while True:
    #Grab single frame of video
    ret,frame=cap.read()
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    faces = face_classifier.detectMultiScale(gray,1.3,5)

    for (x,y,w,h) in faces:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h,x:x+w]
        roi_gray = cv2.resize(roi_gray,(48,48),interpolation=cv2.INTER_AREA)
    # rect,face,image = face_detector(frame)


        if np.sum([roi_gray])!=0:
            roi = roi_gray.astype('float')/255.0
            roi = img_to_array(roi)
            roi = np.expand_dims(roi,axis=0)

        # make a prediction on the ROI, then lookup the class

            preds = classifier.predict(roi)[0]
            label=class_labels[preds.argmax()]
            label_position = (x,y)
            cv2.putText(frame,label,label_position,cv2.FONT_HERSHEY_SIMPLEX,2,(0,255,0),3)
        else:
            cv2.putText(frame,'No Face Found',(20,60),cv2.FONT_HERSHEY_SIMPLEX,2,(0,255,0),3)
    cv2.imshow('Facial Expression Recognition',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

