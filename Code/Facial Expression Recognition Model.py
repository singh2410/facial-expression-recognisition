#!/usr/bin/env python
# coding: utf-8

# # Facial Expression Recognition Training Model
# #By- Aarush Kumar
# #Dated: May 29,2021

# In[3]:


get_ipython().system('pip install tensorflow')
get_ipython().system('pip install keras')


# In[4]:


import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D
import os


# In[5]:


num_classes=5
img_rows,img_cols=48,48
batch_size=8
train_data=r'/home/aarush100616/Downloads/Projects/facialexpressionrec/train'
validation_data=r'/home/aarush100616/Downloads/Projects/facialexpressionrec/validation'


# In[6]:


train_data


# In[7]:


validation_data


# In[8]:


train_datagen=ImageDataGenerator(rescale=1./255, rotation_range=30, shear_range=0.3, 
                                 zoom_range=0.3,width_shift_range=0.4,height_shift_range=0.3,
                                 horizontal_flip=True,vertical_flip=True)

validation_datagen=ImageDataGenerator(rescale=1./255)


# In[9]:


train_generator=train_datagen.flow_from_directory(train_data,color_mode='grayscale', target_size=(img_rows,img_cols), 
                                                 batch_size=batch_size, class_mode='categorical',shuffle=True)


# In[10]:


validation_generator=validation_datagen.flow_from_directory(validation_data,color_mode='grayscale', target_size=(img_rows,img_cols), 
                                                 batch_size=batch_size, class_mode='categorical',shuffle=True)


# In[11]:


model=Sequential()


# In[12]:


#Block-1
model.add(Conv2D(32,(3,3),padding='same',kernel_initializer='he_normal',input_shape=(img_rows,img_cols,1)))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Conv2D(32,(3,3),padding='same',kernel_initializer='he_normal',input_shape=(img_rows,img_cols,1)))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.2))


# In[13]:


# Block-2 
model.add(Conv2D(64,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Conv2D(64,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.2))


# In[14]:


# Block-3
model.add(Conv2D(128,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Conv2D(128,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.2))


# In[15]:


# Block-4 difference in number of neurons 
model.add(Conv2D(256,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Conv2D(256,(3,3),padding='same',kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.2))


# In[16]:


# Block-5 flatten
model.add(Flatten())
model.add(Dense(64,kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))


# In[17]:


# Block-6 Dense
model.add(Dense(64,kernel_initializer='he_normal'))
model.add(Activation('elu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))


# In[18]:


# Block-7 Softmax
model.add(Dense(num_classes,kernel_initializer='he_normal'))
model.add(Activation('softmax'))


# In[19]:


print(model.summary())


# In[20]:


from keras.optimizers import RMSprop,SGD,Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau


# In[21]:


# Save best model
checkpoint = ModelCheckpoint('Emotion_little_vgg.h5',
                             monitor='val_loss',
                             mode='min',
                             save_best_only=True,
                             verbose=1)


# In[22]:


# Monitor validation rounds for 3 times and then stop training
earlystop = EarlyStopping(monitor='val_loss',
                          min_delta=0,
                          patience=3,
                          verbose=1,
                          restore_best_weights=True
                         )


# In[23]:


# If accuracy is not improving for 3 rounds reduce the learning rate
reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                              factor=0.2,
                              patience=3,
                              verbose=1,
                              min_delta=0.0001)


# In[24]:


callbacks = [earlystop,checkpoint,reduce_lr]


# In[25]:


model.compile(loss='categorical_crossentropy',
              optimizer = Adam(lr=0.001),
              metrics=['accuracy'])


# In[26]:


nb_train_samples = 24176
nb_validation_samples = 3006
epochs=25


# In[27]:


history=model.fit_generator(
                train_generator,
                steps_per_epoch=nb_train_samples//batch_size,
                epochs=epochs,
                callbacks=callbacks,
                validation_data=validation_generator,
                validation_steps=nb_validation_samples//batch_size)

