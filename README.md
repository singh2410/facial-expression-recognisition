# Facial Expression Recognisition
## By- Aarush Kumar
Facial Expression Recognisition using Keras & Opencv
In this project I first trained a model using Tensorflow and Keras.In that part I applied some padding and kernel_initializer to initialize an exact fit to my training dataset.Then implemented 'elu' over 'relu' as The exponential linear unit (ELU) with alpha > 0 is: x if x > 0 and alpha * (exp(x) - 1) if x < 0 The ELU hyperparameter alpha controls the value to which an ELU saturates for negative net inputs. ELUs diminish the vanishing gradient effect.Also applied some other functions to model as BatchNormalization,MaxPooling2D so that the model gets well trained.
In order to make my model train in efficient way I implemented checkpoint,earlystop and reduce_lr so that it may stop training if the model accuracy is not improving or the model is taking a lot of time to train.
In my testing model I used casscadeclassifier to load the facial recognition .xml file and finally loaded my model in .h5  format to test it.
## Thankyou! 
